﻿using Microsoft.Identity.Client;
using System;
using System.Activities.Statements;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

//https://www.inkeysolutions.com/blogs/connection-to-dynamics-365-and-sharepoint-authentication-using-clientid-clientsecret-part-1/
//https://github.com/AzureAD/microsoft-authentication-library-for-dotnet/wiki/Username-Password-Authentication#this-flow-is-not-recommended
namespace Dynamics365.Common.Platforms.NetCore
{
    public class XrmConnectAuthentication : DelegatingHandler
    {
        public XrmConnectAuthentication() : base(new HttpClientHandler())
        {
            _authHeader = new AuthenticationHeaderValue("a");
            AuthenticationSilentAsync().Wait();
        }

        //public string ClientId = "<app-reg-guid>";1275b320-25ee-473a-a0fd-0269e2af5894
        public string ClientId = "a1e6bee6-19c5-45c2-92e4-d0665869c08a";

        public string[] Scopes = { "user.read" };

        public async Task AuthenticationSilentAsync()
        {
            AuthenticationResult? authResult = default;
            IPublicClientApplication app = PublicClientApplicationBuilder.Create(ClientId).Build();

            try
            {
                var accounts = await app.GetAccountsAsync();
                authResult = await app.AcquireTokenSilent(Scopes, accounts.FirstOrDefault()).ExecuteAsync();
            }
            catch (MsalUiRequiredException)
            {
                try
                {
                    authResult = await app.AcquireTokenInteractive(Scopes).ExecuteAsync();
                }
                catch (MsalUiRequiredException)
                {
                    // TODO
                }

                // Other errors
            }

            if (authResult is not null)
            {
                _authHeader = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
            }
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Authorization = _authHeader;
            return base.SendAsync(request, cancellationToken);
        }

        private AuthenticationHeaderValue _authHeader;
    }

    public class XrmConnectBase
    {
        //
        public string ServiceUrl = "https://rtowerstest.crm4.dynamics.com";
        //public string ServiceUrl = "https://<domain>.dynamics.com/";
        public string UserAccount = "<user-account>";
        public string Domain = "<server-domain>";
        public string ClientId = "<app-reg-guid>";
        public string[] Scopes = { "User.Read" };

        public void Main()
        {
            var auth = new XrmConnectAuthentication();

            try
            {
                //Create an HTTP client to send a request message to the CRM Web service.
                using (HttpClient httpClient = new HttpClient(auth))
                {
                    //Specify the Web API address of the service and the period of time each request 
                    // has to execute.
                    httpClient.BaseAddress = new Uri(ServiceUrl);
                    httpClient.Timeout = new TimeSpan(0, 2, 0);  //2 minutes

                    //Send the WhoAmI request to the Web API using a GET request. 
                    var response = httpClient.GetAsync("api/data/v8.1/WhoAmI", HttpCompletionOption.ResponseHeadersRead).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //Get the response content and parse it.
                        var contentResult = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Your system user ID is: {0}", contentResult);
                    }
                    else
                    {
                        Console.WriteLine("The request failed with a status of '{0}'",
                               response.ReasonPhrase);
                    }
                }
            }
            catch (Exception ex)
            {
                DisplayException(ex);
                throw;
            }
            finally
            {
                Console.WriteLine("Press <Enter> to exit the program.");
                Console.ReadLine();
            }
        }

        /// <summary> Displays exception information to the console. </summary>
        /// <param name="ex">The exception to output</param>
        private static void DisplayException(Exception ex)
        {
            Console.WriteLine("The application terminated with an error.");
            Console.WriteLine(ex.Message);
            while (ex.InnerException != null)
            {
                Console.WriteLine("\t* {0}", ex.InnerException.Message);
                ex = ex.InnerException;
            }
        }

        //https://docs.microsoft.com/en-us/previous-versions/dynamicscrm-2016/developers-guide/mt779074(v=crm.8)
        //https://docs.microsoft.com/en-us/dynamics365/business-central/dev-itpro/developer/devenv-develop-connect-apps
        //https://api.businesscentral.dynamics.com/v2.0/<environment name>/api/v2.0

    }
}
