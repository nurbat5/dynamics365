﻿using Dynamics365.Abstractions.Interfaces;
using Microsoft.Xrm.Sdk;
using System;

namespace Dynamics365.Sdk
{
    public class XrmEntity : IXrmEntity
    {
        public XrmEntity(Entity entity) => _entity = entity;
        public XrmEntity(string entityName, Guid entityId) => _entity = new Entity(entityName, entityId);

        // TryGetAttribute
        public bool TryGetAttribute(string attributeName, out object value) => _entity.Attributes.TryGetValue(attributeName.ToLower(), out value);
        public bool TryGetAttribute<T>(string attributeName, out T value)
        {
            if(TryGetAttribute(attributeName, out var targetObject) && targetObject is T targetValue)
            {
                value = targetValue;
                return true;
            }

            value = default!;
            return false;
        }

        // GetAttribute
        public object GetAttribute(string attributeName) => TryGetAttribute(attributeName, out var value) ? value : default!;
        public T GetAttribute<T>(string attributeName) => TryGetAttribute<T>(attributeName, out var value) ? value : default!;

        // SetAttribute
        public void SetAttribute<T>(string attributeName, T attributeValue) => _entity.Attributes[attributeName.ToLower()] = attributeValue;
        public object this[string attributeName]
        {
            get => GetAttribute(attributeName);
            set => SetAttribute(attributeName, value);
        }

        // implicit operator
        static public implicit operator XrmEntity(Entity entity) => new(entity);
        static public implicit operator Entity(XrmEntity xrmEntity) => xrmEntity._entity;

        // private
        private readonly Entity _entity;
    }
}
