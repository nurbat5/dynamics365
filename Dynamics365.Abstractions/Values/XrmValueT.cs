﻿using Dynamics365.Abstractions.Interfaces;

namespace Dynamics365.Abstractions.Values
{
    public class XrmValue<T> : XrmValue
    {
        protected XrmValue(IXrmEntity xrmEntity, string attributeName, T attributeValue) : base(xrmEntity, attributeName, attributeValue!) { }
        protected XrmValue(IXrmEntity xrmEntity, string attributeName) : base(xrmEntity, attributeName) { }

        public bool HasValue => AttributeValue is not null;
        public T Value
        {
            get
            {
                if (AttributeValue is not null && AttributeValue is T value) return value;
                return default!;
            }
            set => AttributeValue = value!;
        }
    }
}
