﻿using Dynamics365.Abstractions.Interfaces;

namespace Dynamics365.Abstractions.Values
{
    public class XrmValueInt : XrmValue<int>
    {
        protected XrmValueInt(IXrmEntity xrmEntity, string attributeName) : base(xrmEntity, attributeName) { }
        protected XrmValueInt(IXrmEntity xrmEntity, string attributeName, int attributeValue) : base(xrmEntity, attributeName, attributeValue) { }
    }
}
