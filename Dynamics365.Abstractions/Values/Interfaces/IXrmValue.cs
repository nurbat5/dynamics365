﻿namespace Dynamics365.Abstractions.Values.Interfaces
{
    public interface IXrmValue
    {
        public string AttributeName { get; }
        public object AttributeValue { get; set; }
    }
}
